<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BLog;

class BlogController extends Controller
{
    public function index(){
        $blogs = Blog::all();
    	return view('index',['blogs' => $blogs]);
    }

    public function show($id){
    	$blog = Blog::find($id);
    	return view('show', ['blog' => $blog]);
    }

    public function new(){
        $blog = new Blog();
        return  view('create', ['blog' => $blog]);
    }

    public function create(Request $request){        
        $this->validate($request, [
                'title' => 'required',
                'body' => 'required'
            ]);
        $blog = new Blog(['title' => $request->input('title'), 'body' => $request->input('body')]);
        $blog->save();
        return redirect()->route('blogs.index')->with('title','the new title is : '.$request->input('title'));
    }

    public function edit($id){
        $blog = Blog::find($id);
        return view('edit', ['blog' => $blog]);
    }

    public function update(Request $request){
        $blog = Blog::find($request->input('id'));
        $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'id' => 'required'
            ]);
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->save();
        return redirect()->route('blogs.index')->with('title','the updated title is : '. $request->input('title'));
    }

    public function destroy($id){
        $blog = Blog::find($id);
        $blog->delete();
        return redirect()->route('blogs.index')->with('title', $blog->title.' is deleted');
    }
}
