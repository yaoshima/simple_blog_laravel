<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', ['uses' => 'BlogController@index', 'as' => 'blogs.index']);

Route::get('/blog/{id}', ['uses' => 'BlogController@show', 'as' => 'blogs.show']);

Route::get('/new', ['uses' => 'BlogController@new', 'as' => 'blogs.new']);

Route::get('/edit/{id}',['uses' => 'BlogController@edit', 'as' => 'blogs.edit']);

Route::post('/edit', ['uses' => 'BlogController@update', 'as' => 'blogs.update']);

Route::post('/', ['uses' => 'BlogController@create', 'as' => 'blogs.create']);

Route::get('/delete/{id}', ['uses' => 'BlogController@destroy', 'as' => 'blogs.destroy']);

