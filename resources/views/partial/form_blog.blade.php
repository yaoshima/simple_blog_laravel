<form action="{{$blog->exists ? route('blogs.update') : route('blogs.create') }}" method="POST">
	{{csrf_field()}}
	@if($blog->exists)
		<input type="hidden" name="id" value="{{$blog->id}}">
	@endif
	<div class="form-group">
		<label for="title"></label>
		<input class="form-control" type="text" name="title" id="title" value="{{$blog->title}}">
	</div>
	<div class="form-group">
		<label for="body"></label>
		<textarea class="form-control" name="body" id="body">{{$blog->body}}</textarea>
	</div>

	
	<button class="btn btn-info" type="submit">Done</button>
</form>