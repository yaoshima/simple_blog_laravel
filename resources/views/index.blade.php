@extends('master')

@section('content')
	@include('partial.header_admin')
	
	<div class="container">
		<h1 class="fancy-title">Blog posts</h1>

		@if(Session::has('title'))
		<div class="alert alert-success" role="alert">
		  {{Session::get('title')}}
		</div>
		@endif

		@foreach($blogs as $blog)
			<div class="row">
				<div class="col-12">
					<p>{{$blog->title}}
						<span><a href="{{route('blogs.edit',['id' => $blog->id])}}"> Edit</a></span>
						<span><a href="{{route('blogs.destroy',['id' => $blog->id])}}"> Delete</a></span>
					</p>
					<a href="{{route('blogs.show',['id' => $blog->id])}}">Read more...</a>
				</div>
			</div>
			<hr>
		@endforeach
	</div>
	
@endsection