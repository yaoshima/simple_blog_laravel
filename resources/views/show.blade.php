@extends('master')

@section('content')
	@include('partial.header')
	
	<div class="container">
		<h1 class="fancy-title">{{$blog->title}}</h1>
		<p>{{$blog->body}}</p>
	</div>
@endsection